import React from 'react'
import { configure, addDecorator } from '@storybook/react'
import { muiTheme } from 'storybook-addon-material-ui'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension'
import { BrowserRouter as Router } from 'react-router-dom'

import reducers from '../src/reducers'
const middlewares = [thunk]

const store = createStore(
  reducers,
  composeWithDevTools(applyMiddleware(...middlewares))
)

addDecorator(muiTheme())

addDecorator(getStory => (
  <Provider store={store}>
    <Router>{getStory()}</Router>
  </Provider>
))

configure(() => require('../src/stories'), module)
