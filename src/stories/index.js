import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import { linkTo } from '@storybook/addon-links'
import StoryRouter from 'storybook-router'

import Dealer from '../pages/Dealer'
import Player from '../pages/Player'
import Result from '../pages/Result'

export default storiesOf('Page ', module)
  .addDecorator(
    StoryRouter({
      '/': linkTo('Page', 'Main'),
      '/dealer': linkTo('Page', 'Dealer'),
      '/player': linkTo('Page', 'Player')
    })
  )

  .add('Dealer', () => {
    const props = {
      Draw: e => action('Draw')(e)
    }

    return <Dealer {...props} />
  })

  .add('Results', () => {
    return <Result />
  })

  .add('Player', () => {
    return <Player />
  })
