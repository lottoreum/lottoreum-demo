import React, { Component } from 'react'
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch
} from 'react-router-dom'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'

import Main from './pages/Main'
import Dealer from './pages/Dealer'
import Player from './pages/Player'
import Result from './pages/Result'

class App extends Component {
  render() {
    return (
      <div style={{ margin: '0 auto' }}>
        <Router>
          <Switch>
            <Route exact path="/" component={Main} />
            <Route exact path="/dealer" component={Dealer} />
            <Route exact path="/player" component={Player} />
            <Redirect from="*" to="/" />
          </Switch>
        </Router>
        <div style={{ marginTop: '50px' }}>
          <Result />
        </div>
      </div>
    )
  }
}

export default App
