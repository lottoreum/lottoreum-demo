import Web3 from 'web3'

const contractAddress = '0x592fe0c1a30759599f0f572Bda98a6AAEf1105A1'

const abi = [
  {
    constant: false,
    inputs: [],
    name: 'renounceOwnership',
    outputs: [],
    payable: false,
    stateMutability: 'nonpayable',
    type: 'function'
  },
  {
    constant: true,
    inputs: [],
    name: 'owner',
    outputs: [
      {
        name: '',
        type: 'address'
      }
    ],
    payable: false,
    stateMutability: 'view',
    type: 'function'
  },
  {
    constant: false,
    inputs: [
      {
        name: '_newOwner',
        type: 'address'
      }
    ],
    name: 'transferOwnership',
    outputs: [],
    payable: false,
    stateMutability: 'nonpayable',
    type: 'function'
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: false,
        name: 'owner',
        type: 'address'
      },
      {
        indexed: false,
        name: 'number',
        type: 'string'
      }
    ],
    name: 'LottoPurchase',
    type: 'event'
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: false,
        name: 'number',
        type: 'string'
      },
      {
        indexed: false,
        name: 'winner',
        type: 'address'
      },
      {
        indexed: false,
        name: 'prize',
        type: 'uint256'
      }
    ],
    name: 'LuckyDraw',
    type: 'event'
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        name: 'previousOwner',
        type: 'address'
      }
    ],
    name: 'OwnershipRenounced',
    type: 'event'
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        name: 'previousOwner',
        type: 'address'
      },
      {
        indexed: true,
        name: 'newOwner',
        type: 'address'
      }
    ],
    name: 'OwnershipTransferred',
    type: 'event'
  },
  {
    constant: true,
    inputs: [
      {
        name: '_owner',
        type: 'address'
      }
    ],
    name: 'balanceOf',
    outputs: [
      {
        name: '',
        type: 'uint256'
      }
    ],
    payable: false,
    stateMutability: 'view',
    type: 'function'
  },
  {
    constant: true,
    inputs: [
      {
        name: 'number',
        type: 'string'
      }
    ],
    name: 'ownerOf',
    outputs: [
      {
        name: '',
        type: 'address'
      }
    ],
    payable: false,
    stateMutability: 'view',
    type: 'function'
  },
  {
    constant: false,
    inputs: [
      {
        name: '_number',
        type: 'string'
      }
    ],
    name: 'createLotto',
    outputs: [],
    payable: true,
    stateMutability: 'payable',
    type: 'function'
  },
  {
    constant: false,
    inputs: [
      {
        name: '_number',
        type: 'string'
      }
    ],
    name: 'setLuckyNumber',
    outputs: [],
    payable: false,
    stateMutability: 'nonpayable',
    type: 'function'
  },
  {
    constant: false,
    inputs: [
      {
        name: '_price',
        type: 'uint256'
      }
    ],
    name: 'setLottoPrice',
    outputs: [],
    payable: false,
    stateMutability: 'nonpayable',
    type: 'function'
  }
]

web3 = new Web3(web3.currentProvider)

function getAccount() {
  return web3.eth.getAccounts().then(([account]) => Promise.resolve(account))
}

const Contract = new web3.eth.Contract(abi, contractAddress)
const methods = Contract.methods
const events = Contract.events

export { methods, events, getAccount }
