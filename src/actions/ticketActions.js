import {
  methods,
  events,
  getAccount,
  CREATE_TICKET,
  SET_LUCKY_NUMBER,
  GET_RESULT
} from '../config'

export const CreateTicket = ticket => {
  return dispatch => {
    getAccount().then(account => {
      const options = { from: account }

      return methods
        .createLotto(ticket.item.toString())
        .send(options, (error, result) => {
          if (!error) {
            dispatch({
              type: CREATE_TICKET,
              payload: result
            })
          }
        })
    })
  }
}

export const Draw = ticket => {
  return dispatch => {
    getAccount().then(account => {
      const options = { from: account }

      return methods
        .setLuckyNumber(ticket.item.toString())
        .send(options, (error, result) => {
          if (!error) {
            dispatch({
              type: SET_LUCKY_NUMBER,
              payload: result
            })
          }
        })
    })
  }
  return {
    type: DRAW,
    payload: ticket
  }
  /*methods.createPrize().call({ ...ticket }, (error, result) => {
    if (!error) {
      return {
        type: DRAW,
        payload: result
      }
    }
  })*/
}

export const GetResult = () => {
  return {
    type: GET_RESULT
  }
  /*const params = {
    fromBlock: 3,
    toBlock: 'latest'
  }

  events
    .LuckyDraw(params, (error, event) => {
      console.log(error, event)
      if (!error) {
      return {
        type: GET_RESULT,
        payload: event
      }
    }
    })
    .on('data', function(event) {
      console.log(event) // same results as the optional callback above
    })
    .on('changed', function(event) {
      // remove event from local database
    })
    .on('error', console.error)*/
}
