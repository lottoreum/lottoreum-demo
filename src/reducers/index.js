import { combineReducers } from 'redux'
import result from './resultReducers'

const reducers = {
  result
}

export default combineReducers(reducers)
