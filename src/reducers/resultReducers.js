import { GET_RESULT, CREATE_TICKET, SET_LUCKY_NUMBER } from '../config'

const init = {
  ticket: '',
  transactions: [
    {
      date: 1535208491,
      item: 99,
      address: '0xabcd'
    },
    {
      date: 1535206491,
      item: 2,
      address: '0xfghr'
    },
    {
      date: 1535204491,
      item: 16,
      address: '0xwrrtyy'
    },
    {
      date: 1535204491,
      item: 6,
      address: ''
    }
  ],
  lucky: ''
}

export default function reducer(state = init, action) {
  switch (action.type) {
    case CREATE_TICKET: {
      return Object.assign({}, state, { ticket: action.payload })
    }

    case SET_LUCKY_NUMBER: {
      return Object.assign({}, state, { lucky: action.payload })
    }

    case GET_RESULT: {
      return state
      //return Object.assign({}, state, action.payload.result)
    }

    default:
      return state
  }
}
