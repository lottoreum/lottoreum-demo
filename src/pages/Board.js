import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemSecondaryAction from '@material-ui/core/'
import Chip from '@material-ui/core/Chip'

export const Board = ({ boards }) => (
  <Wrapper>
    <Card>
      <CardContent>
        <List>
          {boards.map(board => (
            <ListItem key={board.item} dense button>
              <ListItemText primary={board.dealer} />
              {board.items.map(item => <Chip label={item} />)}
            </ListItem>
          ))}
        </List>
      </CardContent>
    </Card>
  </Wrapper>
)

Board.propTypes = {
  board: PropTypes.string.isRequired
}

const Wrapper = styled.div`
  width: 40%;
`

export default Board
