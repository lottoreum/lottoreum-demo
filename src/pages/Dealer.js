import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Paper from '@material-ui/core/Paper'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'

import { Draw } from '../actions'

export class Dealer extends Component {
  constructor() {
    super()

    this.state = {
      item: 0
    }

    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleChange = this.handleChange.bind(this)
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value
    })
  }

  handleSubmit(e) {
    e.preventDefault()
    const { item } = this.state

    const ticket = {
      item
    }

    this.props.Draw(ticket)
  }

  render() {
    return (
      <Card style={{ width: '400px', margin: '0 auto' }}>
        <CardContent>
          <form noValidate autoComplete="off" onSubmit={this.handleSubmit}>
            <TextField
              id="item"
              name="item"
              label="Number"
              margin="normal"
              type="number"
              fullWidth={true}
              onChange={this.handleChange('item')}
            />
            <Button
              color="primary"
              fullWidth={true}
              onClick={this.handleSubmit}
            >
              Set Lucky Number
            </Button>
          </form>
        </CardContent>
      </Card>
    )
  }
}

Dealer.propTypes = {
  Draw: PropTypes.func.isRequired
}

export const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      Draw
    },
    dispatch
  )
}

export default connect(null, mapDispatchToProps)(Dealer)
