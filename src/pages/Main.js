import React from 'react'
import PropTypes from 'prop-types'
import Button from '@material-ui/core/Button'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

export const Main = ({ BeDealer, BePlayer }) => (
  <Wrapper>
    <Button variant="outlined">
      <Link to="/dealer">Be A Dealer</Link>
    </Button>
    <Button variant="outlined" color="primary">
      <Link to="/player">Be A Player</Link>
    </Button>
  </Wrapper>
)

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 50px;
`

export default Main
