import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'

import { GetResult } from '../actions'

export class Result extends Component {
  constructor() {
    super()
  }

  componentWillMount() {
    this.props.GetResult()
  }

  render() {
    const { results } = this.props

    return (
      <Table style={{ width: '800px', margin: '0 auto' }}>
        <TableHead>
          <TableRow>
            <TableCell>Date/Time</TableCell>
            <TableCell numeric>Lucky Number</TableCell>
            <TableCell numeric>Address</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {results.map((row, index) => {
            return (
              <TableRow key={index}>
                <TableCell>{new Date(row.date * 1000).toISOString()}</TableCell>
                <TableCell numeric>{row.item}</TableCell>
                <TableCell numeric>{row.address}</TableCell>
              </TableRow>
            )
          })}
        </TableBody>
      </Table>
    )
  }
}

Result.propTypes = {
  results: PropTypes.array.isRequired
}

export const mapStateToProps = state => ({
  results: state.result.transactions
})

export const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      GetResult
    },
    dispatch
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(Result)
